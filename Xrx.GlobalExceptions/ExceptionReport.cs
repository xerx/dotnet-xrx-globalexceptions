﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xrx.GlobalExceptions
{
    public class ExceptionReport
    {
        public DateTime Date { get; set; }
        public string StackTrace { get; set; }
    }
}
