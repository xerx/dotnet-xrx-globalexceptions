﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xrx.GlobalExceptions.Abstractions;

namespace Xrx.GlobalExceptions
{
    public class ExceptionConsumer : IExceptionConsumer
    {
        protected readonly IExceptionCacher exceptionCacher;
        protected readonly IExceptionPresenter exceptionReporter;

        public ExceptionConsumer(IExceptionCacher exceptionCacher,
                                 IExceptionPresenter exceptionReporter)
        {
            this.exceptionCacher = exceptionCacher;
            this.exceptionReporter = exceptionReporter;
            InitializeListeners();
        }
        public void Report()
        {
            IList<ExceptionReport> exceptions = exceptionCacher?.GetExceptions();
            if(exceptions != null)
            {
                exceptionReporter?.PresentExceptions(exceptions);
            }
        }
        protected virtual void InitializeListeners()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainUnhandledException;
            TaskScheduler.UnobservedTaskException += TaskSchedulerUnobservedTaskException;
        }
        private void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e) =>
            OnException(e.ExceptionObject as Exception);

        private void TaskSchedulerUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e) =>
            OnException(e.Exception);

        protected void OnException(Exception exception)
        {
            exceptionCacher?.Handle(exception);
        }

        public void Dispose()
        {
            AppDomain.CurrentDomain.UnhandledException -= CurrentDomainUnhandledException;
            TaskScheduler.UnobservedTaskException -= TaskSchedulerUnobservedTaskException;
        }

    }
}
