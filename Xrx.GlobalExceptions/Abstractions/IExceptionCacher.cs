﻿using System;
using System.Collections.Generic;

namespace Xrx.GlobalExceptions.Abstractions
{
    public interface IExceptionCacher
    {
        void Handle(Exception exception);
        IList<ExceptionReport> GetExceptions();
    }
}
