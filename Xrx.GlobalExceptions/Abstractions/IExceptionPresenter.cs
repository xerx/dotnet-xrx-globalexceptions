﻿using System.Collections.Generic;

namespace Xrx.GlobalExceptions.Abstractions
{
    public interface IExceptionPresenter
    {
        void PresentExceptions(IList<ExceptionReport> exceptions);
    }
}
